package org.growingstems.frctactics;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PreMatchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreMatchFragment extends Fragment {


    private MatchActivity _parent;

    private boolean displayed;

    private Button swap_sides;

    private ImageButton near_switch;
    private ImageButton scale;
    private ImageButton far_switch;

    private TextView clock;
    private Button startB;

    private int defaultColor = 0;

    public PreMatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PreMatchFragment.
     */
    public static PreMatchFragment newInstance(MatchActivity parent) {
        PreMatchFragment fragment = new PreMatchFragment();
        fragment._parent = parent;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prematch, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getGUIRefs(view);

        setListeners();
        displayed = true;

    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    public void saveData() {
        // DO nothing
    }

    public void loadData() {
        if (getView() == null || _parent == null || !displayed)
            return;

        if (!_parent.we_are_red)
            scale.setScaleY(-1f);
        else
            scale.setScaleY(1f);

        scale.setScaleX((_parent.scale_right == _parent.we_are_red) ? -1f : 1f);

        near_switch.setScaleX((_parent.near_switch_right == _parent.we_are_red) ? -1f : 1f);
        far_switch.setScaleX((_parent.far_switch_right == _parent.we_are_red) ? -1f : 1f);


    }

    public void getGUIRefs(View view) {
        near_switch = view.findViewById(R.id.nearSwitchB);
        far_switch = view.findViewById(R.id.farSwitchB);
        scale = view.findViewById(R.id.scaleB);
        swap_sides = view.findViewById(R.id.switchSidesB);
        clock = view.findViewById(R.id.prematchClock);
        startB = view.findViewById(R.id.StartB);
        defaultColor = clock.getCurrentTextColor();
    }

    public void setListeners() {
        near_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _parent.near_switch_right = !_parent.near_switch_right;
                near_switch.setScaleX((_parent.near_switch_right == _parent.we_are_red) ? -1f : 1f);
                _parent.far_switch_right = !_parent.far_switch_right;
                far_switch.setScaleX((_parent.far_switch_right == _parent.we_are_red) ? -1f : 1f);
            }
        });

        far_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _parent.near_switch_right = !_parent.near_switch_right;
                near_switch.setScaleX((_parent.near_switch_right == _parent.we_are_red) ? -1f : 1f);
                _parent.far_switch_right = !_parent.far_switch_right;
                far_switch.setScaleX((_parent.far_switch_right == _parent.we_are_red) ? -1f : 1f);
            }
        });

        scale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _parent.scale_right = !_parent.scale_right;
                scale.setScaleX((_parent.scale_right == _parent.we_are_red) ? -1f : 1f);
            }
        });

        swap_sides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _parent.we_are_red = !_parent.we_are_red;
                loadData();
            }
        });

        startB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_parent.timerRunning) {
                    _parent.reset();
                    _parent.timer = new CountDownTimer(150000, 500) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            _parent.onTick(millisUntilFinished);
                        }

                        @Override
                        public void onFinish() {
                            _parent.timerRunning = false;
                        }
                    };
                    _parent.timer.start();
                    _parent.timerRunning = true;
                    startB.setText("Stop Match");
                    clock.setTextColor(Color.MAGENTA);

                } else {
                    _parent.timer.cancel();
                    _parent.timerRunning = false;
                    startB.setText("Start Match");
                    clock.setText("2:30");
                    clock.setTextColor(defaultColor);
                }
            }
        });

    }

    public void setTimeLeft(long minutes, long seconds) {
        if (getView() == null || _parent == null || !displayed)
            return;
        clock.setText(String.valueOf(minutes) + (seconds >= 10 ? ":" : ":0") + String.valueOf(seconds));
        if (minutes == 2 && seconds > 15) {
            clock.setTextColor(Color.MAGENTA);
        } else if (minutes == 0 && seconds <= 30) {
            if (seconds == 0)
                clock.setTextColor(Color.RED);
            else
                clock.setTextColor(Color.YELLOW);
        } else {
            clock.setTextColor(defaultColor);
        }
    }


}
