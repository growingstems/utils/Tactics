package org.growingstems.frctactics;


import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayDeque;
import java.util.Queue;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MatchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MatchFragment extends Fragment {


    private MatchActivity _parent;

    private boolean displayed;

    private ImageButton farBoost;
    private ImageButton farLevitate;
    private ImageButton farForce;
    private ImageButton farLeft;
    private ImageButton farRight;
    private ImageButton scaleLeft;
    private ImageButton scaleRight;
    private ImageButton nearLeft;
    private ImageButton nearRight;
    private ImageButton nearBoost;
    private ImageButton nearLevitate;
    private ImageButton nearForce;

    private TextView clock;
    private TextView ourScore;
    private TextView theirScore;
    private TextView ourForecast;
    private TextView theirForecast;

    private boolean nearOurControl = false;
    private boolean nearTheirControl = false;
    private boolean scaleOurControl = false;
    private boolean scaleTheirControl = false;
    private boolean farOurControl = false;
    private boolean farTheirControl = false;

    private boolean farBoostActivated = false;
    private long farBoostActivatedAt = 200000;
    private int farBoostLevel = 0;
    private int farBoostActiveLevel = 0;
    private boolean farForceActivated = false;
    private long farForceActivatedAt = 200000;
    private int farForceLevel = 0;
    private int farForceActiveLevel = 0;
    private boolean farLevitateActivated = false;
    private int farLevitateLevel = 0;
    private boolean nearBoostActivated = false;
    private long nearBoostActivatedAt = 200000;
    private int nearBoostLevel = 0;
    private int nearBoostActiveLevel = 0;
    private boolean nearForceActivated = false;
    private long nearForceActivatedAt = 200000;
    private int nearForceLevel = 0;
    private int nearForceActiveLevel = 0;
    private boolean nearLevitateActivated = false;
    private int nearLevitateLevel = 0;

    private int ourSwitchScore = 0;
    private int ourScaleScore = 0;
    private int theirSwitchScore = 0;
    private int theirScaleScore = 0;

    private int ourVaultScore = 0;
    private int theirVaultScore = 0;
    private int ourBoostScore = 0;
    private int theirBoostScore = 0;
    private int ourForceScore = 0;
    private int theirForceScore = 0;

    private enum POWER_UP {FAR_BOOST, FAR_FORCE, NEAR_BOOST, NEAR_FORCE}

    private boolean powerupActive = false;

    private Queue<POWER_UP> powerupQ = new ArrayDeque<>(4);

    private static final int AUTO_RUN_ASSUMED = 15;

    private int defaultColor = 0;

    private static final int CORNFLOWERBLUE = 0xff6495ed;
    private static final int SALMON = 0xfffa8072;


    public MatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MatchFragment.
     */
    public static MatchFragment newInstance(MatchActivity parent) {
        MatchFragment fragment = new MatchFragment();
        fragment._parent = parent;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        getGUIRefs(view);

        setListeners();
        displayed = true;
        defaultColor = clock.getCurrentTextColor();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onPause() {
        super.onPause();
        saveData();
    }

    public void saveData() {
        //do nothing
    }

    public void loadData() {
        if (getView() == null || _parent == null || !displayed)
            return;

        if (!_parent.we_are_red) {
            scaleLeft.setScaleY(-1f);
            scaleRight.setScaleY(-1f);
            ourScore.setTextColor(Color.BLUE);
            ourForecast.setTextColor(CORNFLOWERBLUE);
            theirScore.setTextColor(Color.RED);
            theirForecast.setTextColor(SALMON);
        } else {
            scaleLeft.setScaleY(1f);
            scaleRight.setScaleY(1f);
            theirScore.setTextColor(Color.BLUE);
            theirForecast.setTextColor(CORNFLOWERBLUE);
            ourScore.setTextColor(Color.RED);
            ourForecast.setTextColor(SALMON);
        }

        if (_parent.scale_right == _parent.we_are_red) {
            scaleLeft.setImageDrawable(_parent.getDrawable(R.drawable.scale_blue));
            scaleRight.setImageDrawable(_parent.getDrawable(R.drawable.scale_red));
            scaleLeft.setScaleX(-1f);
            scaleRight.setScaleX(-1f);
        } else {
            scaleLeft.setImageDrawable(_parent.getDrawable(R.drawable.scale_red));
            scaleRight.setImageDrawable(_parent.getDrawable(R.drawable.scale_blue));
            scaleLeft.setScaleX(1f);
            scaleRight.setScaleX(1f);
        }

        if (_parent.far_switch_right == _parent.we_are_red) {
            farLeft.setImageDrawable(_parent.getDrawable(R.drawable.switch_blue));
            farRight.setImageDrawable(_parent.getDrawable(R.drawable.switch_red));
            farLeft.setScaleX(-1f);
            farRight.setScaleX(-1f);
        } else {
            farLeft.setImageDrawable(_parent.getDrawable(R.drawable.switch_red));
            farRight.setImageDrawable(_parent.getDrawable(R.drawable.switch_blue));
            farLeft.setScaleX(1f);
            farRight.setScaleX(1f);
        }

        if (_parent.near_switch_right == _parent.we_are_red) {
            nearLeft.setImageDrawable(_parent.getDrawable(R.drawable.switch_blue));
            nearRight.setImageDrawable(_parent.getDrawable(R.drawable.switch_red));
            nearLeft.setScaleX(-1f);
            nearRight.setScaleX(-1f);
        } else {
            nearLeft.setImageDrawable(_parent.getDrawable(R.drawable.switch_red));
            nearRight.setImageDrawable(_parent.getDrawable(R.drawable.switch_blue));
            nearLeft.setScaleX(1f);
            nearRight.setScaleX(1f);
        }

    }

    public void getGUIRefs(View view) {
        farBoost = view.findViewById(R.id.farBoost);
        farLevitate = view.findViewById(R.id.farLevitate);
        farForce = view.findViewById(R.id.farForce);

        farLeft = view.findViewById(R.id.farSwitchLeft);
        farRight = view.findViewById(R.id.farSwitchRight);
        scaleLeft = view.findViewById(R.id.scaleLeft);
        scaleRight = view.findViewById(R.id.scaleRight);
        nearLeft = view.findViewById(R.id.nearSwitchLeftB);
        nearRight = view.findViewById(R.id.nearSwitchRightB);

        nearBoost = view.findViewById(R.id.nearBoost);
        nearLevitate = view.findViewById(R.id.nearLevitate);
        nearForce = view.findViewById(R.id.nearForce);

        clock = view.findViewById(R.id.matchClock);
        ourScore = view.findViewById(R.id.ourScore);
        ourForecast = view.findViewById(R.id.ourForecast);
        theirScore = view.findViewById(R.id.theirScore);
        theirForecast = view.findViewById(R.id.theirForecast);
    }

    private void vaultListeners() {

        farBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farBoostLevel++;
                if (farBoostLevel > 3)
                    farBoostLevel = 3;
                else
                    theirVaultScore += 5;
                switch (farBoostLevel) {
                    case 1:
                        farBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_1));
                        break;
                    case 2:
                        farBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_2));
                        break;
                    case 3:
                        farBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_3));
                        break;
                }
            }
        });

        farLevitate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farLevitateLevel++;
                if (farLevitateLevel > 3)
                    farLevitateLevel = 3;
                else
                    theirVaultScore += 5;
                switch (farLevitateLevel) {
                    case 1:
                        farLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_1));
                        break;
                    case 2:
                        farLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_2));
                        break;
                    case 3:
                        farLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_3));
                        break;
                }
            }
        });

        farForce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farForceLevel++;
                if (farForceLevel > 3)
                    farForceLevel = 3;
                else
                    theirVaultScore += 5;
                switch (farForceLevel) {
                    case 1:
                        farForce.setImageDrawable(_parent.getDrawable(R.drawable.force_1));
                        break;
                    case 2:
                        farForce.setImageDrawable(_parent.getDrawable(R.drawable.force_2));
                        break;
                    case 3:
                        farForce.setImageDrawable(_parent.getDrawable(R.drawable.force_3));
                        break;
                }
            }
        });

        nearBoost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nearBoostLevel++;
                if (nearBoostLevel > 3)
                    nearBoostLevel = 3;
                else
                    theirVaultScore += 5;
                switch (nearBoostLevel) {
                    case 1:
                        nearBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_1));
                        break;
                    case 2:
                        nearBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_2));
                        break;
                    case 3:
                        nearBoost.setImageDrawable(_parent.getDrawable(R.drawable.boost_3));
                        break;
                }
            }
        });

        nearLevitate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nearLevitateLevel++;
                if (nearLevitateLevel > 3)
                    nearLevitateLevel = 3;
                else
                    theirVaultScore += 5;
                switch (nearLevitateLevel) {
                    case 1:
                        nearLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_1));
                        break;
                    case 2:
                        nearLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_2));
                        break;
                    case 3:
                        nearLevitate.setImageDrawable(_parent.getDrawable(R.drawable.levitate_3));
                        break;
                }
            }
        });

        nearForce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nearForceLevel++;
                if (nearForceLevel > 3)
                    nearForceLevel = 3;
                else
                    theirVaultScore += 5;
                switch (nearForceLevel) {
                    case 1:
                        nearForce.setImageDrawable(_parent.getDrawable(R.drawable.force_1));
                        break;
                    case 2:
                        nearForce.setImageDrawable(_parent.getDrawable(R.drawable.force_2));
                        break;
                    case 3:
                        nearForce.setImageDrawable(_parent.getDrawable(R.drawable.force_3));
                        break;
                }
            }
        });

        farForce.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (farForceActiveLevel == 0 && farForceLevel > 0) {
                    farForceActiveLevel = farForceLevel;
                    farForce.setBackgroundColor(_parent.we_are_red ? Color.BLUE : Color.RED);

                    if (powerupActive) {
                        powerupQ.add(POWER_UP.FAR_FORCE);
                    } else {
                        farForceActivated = true;
                        farForceActivatedAt = _parent.currentTimeLeft;
                    }
                    return true;
                } else if (farForceActiveLevel > 0) {
                    if (!farForceActivated)
                        powerupQ.remove(POWER_UP.FAR_FORCE);
                    farForceActiveLevel = 0;
                    farForceActivated = false;
                    farForceActivatedAt = 200000;
                    farForce.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });

        farBoost.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (farBoostActiveLevel == 0 && farBoostLevel > 0) {
                    farBoostActiveLevel = farBoostLevel;
                    farBoost.setBackgroundColor(_parent.we_are_red ? Color.BLUE : Color.RED);

                    if (powerupActive) {
                        powerupQ.add(POWER_UP.FAR_BOOST);
                    } else {
                        farBoostActivated = true;
                        farBoostActivatedAt = _parent.currentTimeLeft;
                    }
                    return true;
                } else if (farBoostActiveLevel > 0) {
                    if (!farBoostActivated)
                        powerupQ.remove(POWER_UP.FAR_BOOST);
                    farBoostActiveLevel = 0;
                    farBoostActivated = false;
                    farBoostActivatedAt = 200000;
                    farBoost.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });

        farLevitate.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!farLevitateActivated && farLevitateLevel >= 3) {
                    farLevitateActivated = true;
                    farLevitate.setBackgroundColor(_parent.we_are_red ? Color.BLUE : Color.RED);
                    return true;
                } else if (farLevitateActivated) {
                    farLevitateActivated = false;
                    farLevitate.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });

        nearForce.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (nearForceActiveLevel == 0 && nearForceLevel > 0) {
                    nearForceActiveLevel = nearForceLevel;
                    nearForce.setBackgroundColor(!_parent.we_are_red ? Color.BLUE : Color.RED);

                    if (powerupActive) {
                        powerupQ.add(POWER_UP.NEAR_FORCE);
                    } else {
                        nearForceActivated = true;
                        nearForceActivatedAt = _parent.currentTimeLeft;
                    }
                    return true;
                } else if (nearForceActiveLevel > 0) {
                    if (!nearForceActivated)
                        powerupQ.remove(POWER_UP.NEAR_FORCE);
                    nearForceActiveLevel = 0;
                    nearForceActivated = false;
                    nearForceActivatedAt = 200000;
                    nearForce.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });

        nearBoost.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (nearBoostActiveLevel == 0 && nearBoostLevel > 0) {
                    nearBoostActiveLevel = nearBoostLevel;
                    nearBoost.setBackgroundColor(!_parent.we_are_red ? Color.BLUE : Color.RED);

                    if (powerupActive) {
                        powerupQ.add(POWER_UP.NEAR_BOOST);
                    } else {
                        nearBoostActivated = true;
                        nearBoostActivatedAt = _parent.currentTimeLeft;
                    }
                    return true;
                } else if (nearBoostActiveLevel > 0) {
                    if (!nearBoostActivated)
                        powerupQ.remove(POWER_UP.NEAR_BOOST);
                    nearBoostActiveLevel = 0;
                    nearBoostActivated = false;
                    nearBoostActivatedAt = 200000;
                    nearBoost.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });

        nearLevitate.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!nearLevitateActivated && nearLevitateLevel >= 3) {
                    nearLevitateActivated = true;
                    nearLevitate.setBackgroundColor(!_parent.we_are_red ? Color.BLUE : Color.RED);
                    return true;
                } else if (nearLevitateActivated) {
                    nearLevitateActivated = false;
                    nearLevitate.setBackgroundColor(Color.TRANSPARENT);
                    return true;
                }
                return false;
            }
        });
    }

    private void setFarColor() {
        int color = Color.TRANSPARENT;
        if ((_parent.we_are_red && farOurControl) || (!_parent.we_are_red && farTheirControl)) {
            color = Color.RED;
        } else if ((!_parent.we_are_red && farOurControl) || (_parent.we_are_red && farTheirControl)) {
            color = Color.BLUE;
        }
        farLeft.setBackgroundColor(color);
        farRight.setBackgroundColor(color);
    }

    private void setScaleColor() {
        int color = Color.TRANSPARENT;
        if ((_parent.we_are_red && scaleOurControl) || (!_parent.we_are_red && scaleTheirControl)) {
            color = Color.RED;
        } else if ((!_parent.we_are_red && scaleOurControl) || (_parent.we_are_red && scaleTheirControl)) {
            color = Color.BLUE;
        }
        scaleLeft.setBackgroundColor(color);
        scaleRight.setBackgroundColor(color);
    }

    private void setNearColor() {
        int color = Color.TRANSPARENT;
        if ((_parent.we_are_red && nearOurControl) || (!_parent.we_are_red && nearTheirControl)) {
            color = Color.RED;
        } else if ((!_parent.we_are_red && nearOurControl) || (_parent.we_are_red && nearTheirControl)) {
            color = Color.BLUE;
        }
        nearLeft.setBackgroundColor(color);
        nearRight.setBackgroundColor(color);
    }

    private void controlListeners() {
        farLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_parent.far_switch_right) {
                    farTheirControl = !farTheirControl;
                    farOurControl = false;

                }
                else {
                    farOurControl = !farOurControl;
                    farTheirControl = false;
                }
                setFarColor();
            }
        });

        farRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_parent.far_switch_right) {
                    farTheirControl = !farTheirControl;
                    farOurControl = false;

                }
                else {
                    farOurControl = !farOurControl;
                    farTheirControl = false;
                }
                setFarColor();
            }
        });


        scaleLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_parent.scale_right) {
                    scaleTheirControl = !scaleTheirControl;
                    scaleOurControl = false;

                }
                else {
                    scaleOurControl = !scaleOurControl;
                    scaleTheirControl = false;
                }
                setScaleColor();
            }
        });

        scaleRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_parent.scale_right) {
                    scaleTheirControl = !scaleTheirControl;
                    scaleOurControl = false;

                }
                else {
                    scaleOurControl = !scaleOurControl;
                    scaleTheirControl = false;
                }
                setScaleColor();
            }
        });

        nearLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_parent.near_switch_right) {
                    nearTheirControl = !nearTheirControl;
                    nearOurControl = false;

                }
                else {
                    nearOurControl = !nearOurControl;
                    nearTheirControl = false;
                }
                setNearColor();
            }
        });

        nearRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_parent.near_switch_right) {
                    nearTheirControl = !nearTheirControl;
                    nearOurControl = false;

                }
                else {
                    nearOurControl = !nearOurControl;
                    nearTheirControl = false;
                }
                setNearColor();
            }
        });
    }

    public void setListeners() {
        vaultListeners();
        controlListeners();
    }


    public void setTimeLeft(long minutes, long seconds) {
        if (getView() == null || _parent == null || !displayed)
            return;
        clock.setText(String.valueOf(minutes) + (seconds >= 10 ? ":" : ":0") + String.valueOf(seconds));

        int scorepertick = 1;

        if (minutes == 2 && seconds > 15) {
            clock.setTextColor(Color.MAGENTA);
            scorepertick = 2;
        } else if (minutes == 0 && seconds <= 30) {
            if (seconds == 0)
                clock.setTextColor(Color.RED);
            else
                clock.setTextColor(Color.YELLOW);
        } else {
            clock.setTextColor(defaultColor);
        }

        if (powerupActive) {
            if ((farBoostActivated && _parent.currentTimeLeft < (farBoostActivatedAt - 10000))
                    && (nearBoostActivated && _parent.currentTimeLeft < (nearBoostActivatedAt - 10000))
                    && (farForceActivated && _parent.currentTimeLeft < (farForceActivatedAt - 10000))
                    && (nearForceActivated && _parent.currentTimeLeft < (nearForceActivatedAt - 10000))) {
                if (!powerupQ.isEmpty())
                    switch (powerupQ.remove()) {
                        case FAR_BOOST:
                            farBoostActivated = true;
                            farBoostActivatedAt = _parent.currentTimeLeft - 500; //activated prior to this frame
                            break;
                        case FAR_FORCE:
                            farForceActivated = true;
                            farForceActivatedAt = _parent.currentTimeLeft - 500;
                            break;
                        case NEAR_BOOST:
                            nearBoostActivated = true;
                            nearBoostActivatedAt = _parent.currentTimeLeft - 500; //activated prior to this frame
                            break;
                        case NEAR_FORCE:
                            nearForceActivated = true;
                            nearForceActivatedAt = _parent.currentTimeLeft - 500;
                            break;                            
                    }
                else
                    powerupActive = false;
            }
        }



        //Update scores
        if (farTheirControl) {
            theirSwitchScore += scorepertick;
            if (farBoostActivated && _parent.currentTimeLeft >= (farBoostActivatedAt - 10000) && (farBoostActiveLevel & 0x01) > 0 )
                theirBoostScore += 1;
        }
        if (scaleTheirControl && !(nearForceActivated && _parent.currentTimeLeft >= (nearForceActivatedAt - 10000) && (nearForceActiveLevel & 0x02) > 0)) {
            theirScaleScore += scorepertick;
            if (farBoostActivated && _parent.currentTimeLeft >= (farBoostActivatedAt - 10000) && (farBoostActiveLevel & 0x02) > 0 )
                theirBoostScore += 1;
        }

        if (nearOurControl) {
            ourSwitchScore += scorepertick;
            if (nearBoostActivated && _parent.currentTimeLeft >= (nearBoostActivatedAt - 10000) && (nearBoostActiveLevel & 0x01) > 0 )
                ourBoostScore += 1;
        }
        if (scaleOurControl && !(farForceActivated && _parent.currentTimeLeft >= (farForceActivatedAt - 10000) && (farForceActiveLevel & 0x02) > 0)) {
            ourScaleScore += scorepertick;
            if (nearBoostActivated && _parent.currentTimeLeft >= (nearBoostActivatedAt - 10000) && (nearBoostActiveLevel & 0x02) > 0 )
                ourBoostScore += 1;
        }

        if (farForceActivated && _parent.currentTimeLeft >= (farForceActivatedAt - 10000) && (farForceActiveLevel & 0x02) > 0) {
            theirForceScore += 1;
        }

        if (nearForceActivated && _parent.currentTimeLeft >= (nearForceActivatedAt - 10000) && (nearForceActiveLevel & 0x02) > 0) {
            ourForceScore += 1;
        }


        //Doing half-second ticks, so control scores are divided by 2 for display, stored as double
        int ourscore = ourScaleScore + ourSwitchScore + ourBoostScore + ourForceScore;
        ourscore /= 2;
        ourscore += ourVaultScore + (nearLevitateActivated ? 30 : 0);

        ourScore.setText(String.valueOf(ourscore));

        int theirscore = theirScaleScore + theirSwitchScore + theirBoostScore + theirForceScore;
        theirscore /= 2;
        theirscore += theirVaultScore + (farLevitateActivated ? 30 : 0);

        theirScore.setText(String.valueOf(theirscore));


    }

    public void reset() {
        nearOurControl = false;
        nearTheirControl = false;
        scaleOurControl = false;
        scaleTheirControl = false;
        farOurControl = false;
        farTheirControl = false;

        farBoostActivated = false;
        farBoostActivatedAt = 0;
        farBoostLevel = 0;
        farBoostActiveLevel = 0;
        farForceActivated = false;
        farForceActivatedAt = 0;
        farForceLevel = 0;
        farForceActiveLevel = 0;
        farLevitateActivated = false;
        farLevitateLevel = 0;
        nearBoostActivated = false;
        nearBoostActivatedAt = 0;
        nearBoostLevel = 0;
        nearBoostActiveLevel = 0;
        nearForceActivated = false;
        nearForceActivatedAt = 0;
        nearForceLevel = 0;
        nearForceActiveLevel = 0;
        nearLevitateActivated = false;
        nearLevitateLevel = 0;

        ourSwitchScore = 0;
        ourScaleScore = 0;
        theirSwitchScore = 0;
        theirScaleScore = 0;

        ourVaultScore = 0;
        theirVaultScore = 0;
        ourBoostScore = 0;
        theirBoostScore = 0;
        ourForceScore = 0;
        theirForceScore = 0;
    }

}
