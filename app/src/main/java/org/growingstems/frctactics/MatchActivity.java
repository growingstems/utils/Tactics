package org.growingstems.frctactics;

// I am well aware that this code does not follow good practices for inheritance, abstraction, etc. This is in the interest of time....

import android.app.Activity;
import android.app.ActionBar;
import android.app.FragmentTransaction;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

public class MatchActivity extends Activity {

    public static final int NUM_SCREENS = 2;
    public static final int PRE_MATCH_SCREEN = 0;
    public static final int MATCH_SCREEN = 1;

    private MatchViewAdapter mMatchViewAdapter;

    public boolean scale_right = false;
    public boolean near_switch_right = false;
    public boolean far_switch_right = false;

    public boolean we_are_red = false;

    public long currentTimeLeft = 150000;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private int mCurrentPage;

    public CountDownTimer timer;
    public boolean timerRunning = false;

    private PreMatchFragment preMatch = null;
    private MatchFragment match = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mMatchViewAdapter = new MatchViewAdapter(getFragmentManager());
        mCurrentPage = 0;

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mMatchViewAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                pageSelected(position);
            }
        });

        getActionBar().setTitle("Match Start");
    }

    public void pageSelected(int position) {
        if (position == PRE_MATCH_SCREEN) {
            getActionBar().setTitle("Match Start");
            if (match != null) match.saveData();
            if (preMatch != null) preMatch.loadData();
        } else if (position == MATCH_SCREEN) {
            getActionBar().setTitle("Match Running");
            if (preMatch != null) preMatch.saveData();
            if (match != null) match.loadData();
        }
    }

    @Override
    public void onBackPressed() {
        onBack(null);
    }

    public void onBack(View v) {
        if (mCurrentPage == 0 || mCurrentPage >= NUM_SCREENS) {
            finish();
        }
        mViewPager.setCurrentItem(mCurrentPage - 1, true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_match, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class MatchViewAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragments;

        public MatchViewAdapter(FragmentManager fm) {
            super(fm);
            fragments = new SparseArray<Fragment>(NUM_SCREENS);
        }

        @Override
        public Fragment getItem(int position) {
            return getFragment(position);
        }

        public Fragment getFragment(int position) {
            if (fragments.get(position) != null) {
                return fragments.get(position);
            }
            switch (position) {
                case PRE_MATCH_SCREEN:
                    preMatch = PreMatchFragment.newInstance(MatchActivity.this);
                    fragments.put(position, preMatch);
                    return preMatch;
                case MATCH_SCREEN:
                default:
                    match = MatchFragment.newInstance(MatchActivity.this);
                    fragments.put(position, match);
                    return match;
            }
        }

        @Override
        public int getCount() {
            return NUM_SCREENS;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return "";
        }
    }

    public void onTick(long millis) {
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        seconds = seconds % 60;

        currentTimeLeft = millis;

        if (preMatch != null)
            preMatch.setTimeLeft(minutes, seconds);
        if (match != null)
            match.setTimeLeft(minutes, seconds);
    }

    public void reset() {
        match.reset();
        match.loadData();
    }
}
